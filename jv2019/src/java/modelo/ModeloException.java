/** 
Proyecto: Juego de la vida.
 * Clase de excepción para la gestión de los errores 
 * en las clases del modelo.
 * @since: prototipo 0.1.2
 * @source: ModeloException.java 
 * @version: 0.1.2 - 2020/02/25
 * @author: ajp
 */

package modelo;

public class ModeloException extends RuntimeException {

	public ModeloException(String mensaje) {
		super(mensaje);
	}
	
	public ModeloException() {
		super();
	}
}
